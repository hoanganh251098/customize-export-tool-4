const
    puppeteer   = require('puppeteer'),
    https       = require('https'),
    request     = require('request'),
    express     = require('express')
    ;
let exportDataMap = {};
let productDataMap = {};
let resizeDimensions = {
    width: null,
    height: null
};

$('#export-btn').on('click', async function() {
    $('#export-btn').prop('disabled', true);
    setOutput('');
    try {
        await main();
    }
    catch(e) {
        alert(e);
    }
    $('#export-btn').prop('disabled', false);
});
// $('#copy-btn').on('click', function() {
//     printMessage('Copied.');
//     clipboardy.writeSync($('#output').val());
// });

$('select').on('change', e => {
    const value = e.target.value;
    lockResolutionInput(value != 'custom');
    if(value == 'default') {
        setResolutionInput('', '');
    }
    if(value == 'detect') {
        setResolutionInput('', '');
        detect();
    }
    if(value == 'custom') {
        setResolutionInput('', '');
        lockResolutionInput(false);
    }
});

function printMessage(msg) {
    const rowsLimit = +$('#message').attr('rows');
    const finalMessage = $('#message').val() + '\n' + msg;
    $('#message').val(finalMessage.split('\n').slice(-rowsLimit).join('\n'));
}

function setOutput(text) {
    $('#output').val(text);
}

function setResolutionInput(width, height) {
    resizeDimensions.width = width;
    resizeDimensions.height = height;
}

async function detect() {
    const previewImgURL = $('#preview-url-input').val();
    const orderData = exportDataMap[previewImgURL];
    if(!orderData) return;
    const sku = orderData.productName;
    const productDataURL = `https://dev.navitee.com/design-upload-v2/products/${sku}/data.json`;
    printMessage('Detecting ...');
    if(!productDataMap[sku]) {
        productDataMap[sku] = await new Promise(resolve => {
            request(productDataURL, (err, response, body) => {
                resolve(JSON.parse(body));
            });
        });
    }
    const productData = productDataMap[sku];
    if(sku.includes('canvas')) {
        if(productData.document.width > productData.document.height) {
            setResolutionInput(7500, 5000);
            printMessage('Detect result: hcanvas 7500x5000');
            return;
        }
        else {
            setResolutionInput(5000, 7500);
            printMessage('Detect result: hcanvas 5000x7500');
            return;
        }
    }
    setResolutionInput('', '');
    printMessage('Detect result: keep resolution');
}

async function main() {
    let previewImgURL = $('#preview-url-input').val(),
        browser,
        page,
        hashCode,
        requestConfig,
        jsonData,
        productURL,
        finalUpload
        ;

    try {
        hashCode = previewImgURL.match(/(?<=amazonaws\.com\/)[a-zA-Z0-9]+/g)[0];
    }
    catch(e) {
        alert('Failed to get hash code');
        return;
    }

    printMessage('Loading user input ...');
    jsonData = await (new Promise((resolve, reject) => {
        request(`http://dev.navitee.com/design-upload/export_synth/${hashCode}.json`, function(error, response, body) {
            if(error) reject(error);
            resolve(JSON.parse(body));
        });
    }));
    exportDataMap[previewImgURL] = jsonData;

    if($('select').val() == 'detect') {
        detect();
    }

    printMessage('Loading product page ...');
    productURL  = `https://dev.navitee.com/iframe/dist/?product=${jsonData.productName}&mode=debug`;
    browser     = await puppeteer.launch({ headless: true,
        defaultViewport: null, });
    page        = await browser.newPage();
    // await page.setViewport({ width: 1366, height: 768});

    await page.goto(productURL, {waitUntil: 'networkidle2'});

    try {
        printMessage('Fetching user input ...');
        await page.evaluate(async (_) => {
            await App.canvas.import(_);
        }, jsonData);

        await new Promise(resolve => {
            printMessage('Waiting for background ...');
            async function timeoutHandle() {
                const isBgReady = await page.evaluate(async (_) => {
                    let pass = true;
                    App.layers.forEach(layer => {
                        if(!pass) return;
                        if(layer.type == 'fg') {
                            if(!layer.allObjects[0] || !layer.allObjects[1]) pass = false;
                        }
                    });
                    return pass;
                });
                if(isBgReady) {
                    printMessage('Background ready')
                    return resolve();
                }
                setTimeout(timeoutHandle, 1000);
            }
            setTimeout(timeoutHandle, 1000);
        });

        const resizeWidth = $('#width-input').val();
        const resizeHeight = $('#height-input').val();

        const timeoutTimeoutID = setTimeout(() => {
            throw `Upload timeout`;
        }, 150000);

        printMessage('Uploading ...');

        const uploadData = await page.evaluate(async (_) => {
            try {
                const exportedBase64    = await App.canvas.exportBase64();
                if(_.resizeWidth && _.resizeHeight) {
                    return await App.helper.uploadBlobAWS(App.helper.imgResizeToBlob(exportedBase64, { width: +_.resizeWidth, height: +_.resizeHeight }));
                }
                else {
                    return await App.helper.uploadBlobAWS(App.helper.toBlob(exportedBase64));
                }
            }
            catch(e) {
                return { error: e };
            }
        }, { resizeWidth, resizeHeight });

        clearTimeout(timeoutTimeoutID);

        if(uploadData.error) {
            throw uploadData.error;
        }

        setOutput(uploadData.data);

        await browser.close();

        printMessage('Done.');
    }
    catch(e) {
        await browser.close();
        printMessage(`Error: ${JSON.stringify(e)}`);
    }
}

